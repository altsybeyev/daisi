#!/bin/bash

sudo service postgresql stop
docker-compose stop

docker-compose build

nohup docker-compose up cpp db & > /dev/null

sleep 4s
chmod 777 -R ./log
docker exec postgres_daisi_container "/opt/daisi/db/deploy_db.sh"
docker exec daisi_cpp_build_container "/opt/daisi/daisiaccel/docker/buildr.sh"

docker-compose stop