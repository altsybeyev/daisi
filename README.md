# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository includes all files for fullstack development of daisi project using docker container

### workflow ###

1. install docker and docker-compose

2. run prepare_env.sh. It will deploy database and build release cpp services

3. run docker-compose up

4. go to http://127.0.0.1:8000/daisi

5. to start calculation service run

/opt/daisi/daisiaccel_build/release/app/daisiaccel_app_solver /opt/daisi/daisiaccel_build/release/app/config.json

or

/opt/daisi/daisiaccel_build/debug/app/daisiaccel_app_solver /opt/daisi/daisiaccel_build/debug/app/config.json

inside container daisi_cpp_build_container

6. to build cpp service run scripts

/opt/daisi/daisiaccel/docker/buildr.sh

or

/opt/daisi/daisiaccel/docker/buildd.sh

inside container daisi_cpp_build_container