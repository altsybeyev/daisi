--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.0

-- Started on 2020-02-08 11:25:03

ALTER DEFAULT PRIVILEGES IN SCHEMA public
  GRANT SELECT, INSERT, DELETE, UPDATE ON TABLES TO daisi_db_user;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2383 (class 1262 OID 16953)
-- Name: daisi_db; Type: DATABASE; Schema: -; Owner: daisi_db_user
--


\connect daisi_db

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12393)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2385 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 710 (class 1247 OID 17715)
-- Name: return_type; Type: TYPE; Schema: public; Owner: daisi_db_user
--

CREATE TYPE return_type AS (
	label text,
	component_typename text,
	name text
);


--
-- TOC entry 713 (class 1247 OID 17729)
-- Name: return_type1; Type: TYPE; Schema: public; Owner: daisi_db_user
--

CREATE TYPE return_type1 AS (
	component_type text,
	label text
);


--
-- TOC entry 716 (class 1247 OID 17737)
-- Name: return_type2; Type: TYPE; Schema: public; Owner: daisi_db_user
--

CREATE TYPE return_type2 AS (
	component_type text,
	label text,
	name text
);


--
-- TOC entry 707 (class 1247 OID 17329)
-- Name: task_data; Type: TYPE; Schema: public; Owner: daisi_db_user
--

CREATE TYPE task_data AS (
	id integer,
	model_id integer,
	solver_pseudoname text,
	name text
);



--
-- TOC entry 209 (class 1259 OID 17317)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


--
-- TOC entry 208 (class 1259 OID 17315)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: daisi_db_user
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2386 (class 0 OID 0)
-- Dependencies: 208
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: daisi_db_user
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;




SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 16971)
-- Name: lazymodelcomponentsviewsitems; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE lazymodelcomponentsviewsitems (
    id integer NOT NULL,
    model_id integer NOT NULL,
    model_view_item_pattern_id integer NOT NULL,
    content text,
    is_valid boolean,
    component_name text NOT NULL
);

--
-- TOC entry 186 (class 1259 OID 16996)
-- Name: lazymodelcomponentsviewsitems_id_seq; Type: SEQUENCE; Schema: public; Owner: daisi_db_user
--

CREATE SEQUENCE lazymodelcomponentsviewsitems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2387 (class 0 OID 0)
-- Dependencies: 186
-- Name: lazymodelcomponentsviewsitems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: daisi_db_user
--

ALTER SEQUENCE lazymodelcomponentsviewsitems_id_seq OWNED BY lazymodelcomponentsviewsitems.id;


--
-- TOC entry 207 (class 1259 OID 17285)
-- Name: lazymodelcomponentsviewsitems_view; Type: VIEW; Schema: public; Owner: daisi_db_user
--

CREATE VIEW lazymodelcomponentsviewsitems_view AS
 SELECT lazymodelcomponentsviewsitems.component_name
   FROM lazymodelcomponentsviewsitems;



--
-- TOC entry 187 (class 1259 OID 16998)
-- Name: modelcomponentsrelation; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE modelcomponentsrelation (
    model_typename text NOT NULL,
    component_typename text NOT NULL,
    view_typename text NOT NULL,
    name text NOT NULL
);



--
-- TOC entry 188 (class 1259 OID 17004)
-- Name: modelcomponentstypesnames; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE modelcomponentstypesnames (
    pseudoname text NOT NULL
);





--
-- TOC entry 189 (class 1259 OID 17010)
-- Name: modelcomponentsviewslabels; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE modelcomponentsviewslabels (
    pseudoname text NOT NULL,
    label text
);



--
-- TOC entry 190 (class 1259 OID 17016)
-- Name: modelcomponentsviewsplotitempattern; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE modelcomponentsviewsplotitempattern (
    id integer NOT NULL,
    component_type text NOT NULL,
    view_label text,
    item_label text NOT NULL,
    format text,
    xlabel text,
    ylabel text,
    labels character varying[],
    is_editable boolean,
    is_result boolean
);



--
-- TOC entry 191 (class 1259 OID 17022)
-- Name: modelcomponentsviewsplotitempattern_id_seq; Type: SEQUENCE; Schema: public; Owner: daisi_db_user
--

CREATE SEQUENCE modelcomponentsviewsplotitempattern_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2388 (class 0 OID 0)
-- Dependencies: 191
-- Name: modelcomponentsviewsplotitempattern_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: daisi_db_user
--

ALTER SEQUENCE modelcomponentsviewsplotitempattern_id_seq OWNED BY modelcomponentsviewsplotitempattern.id;


--
-- TOC entry 192 (class 1259 OID 17024)
-- Name: modelcomponentsviewsrelation; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE modelcomponentsviewsrelation (
    component_type text NOT NULL,
    label text NOT NULL,
    is_editable boolean
);


--
-- TOC entry 193 (class 1259 OID 17030)
-- Name: modelcomponentsviewssimpleitempattern; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE modelcomponentsviewssimpleitempattern (
    id integer NOT NULL,
    component_typename text NOT NULL,
    view_label text NOT NULL,
    item_typename text,
    item_label text NOT NULL,
    default_content text,
    is_editable boolean,
    is_result boolean
);

CREATE TABLE options (
    description text NOT NULL PRIMARY KEY,
    option integer NOT NULL
);
--
-- TOC entry 194 (class 1259 OID 17036)
-- Name: modelcomponentsviewssimpleitempattern_id_seq; Type: SEQUENCE; Schema: public; Owner: daisi_db_user
--

CREATE SEQUENCE modelcomponentsviewssimpleitempattern_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2389 (class 0 OID 0)
-- Dependencies: 194
-- Name: modelcomponentsviewssimpleitempattern_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: daisi_db_user
--

ALTER SEQUENCE modelcomponentsviewssimpleitempattern_id_seq OWNED BY modelcomponentsviewssimpleitempattern.id;


--
-- TOC entry 195 (class 1259 OID 17038)
-- Name: modelcomponentsviewssimpleitemtypenames; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE modelcomponentsviewssimpleitemtypenames (
    pseudoname text NOT NULL,
    typename text
);


--
-- TOC entry 196 (class 1259 OID 17044)
-- Name: models; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE models (
    id integer NOT NULL,
    label text NOT NULL,
    description text,
    created timestamp without time zone,
    edited timestamp without time zone,
    is_hidden boolean,
    typename text NOT NULL,
    lock_owner_name integer,
    owner_name integer NOT NULL,
    clone_source integer
);


--
-- TOC entry 197 (class 1259 OID 17050)
-- Name: models_id_seq; Type: SEQUENCE; Schema: public; Owner: daisi_db_user
--

CREATE SEQUENCE models_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2390 (class 0 OID 0)
-- Dependencies: 197
-- Name: models_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: daisi_db_user
--

ALTER SEQUENCE models_id_seq OWNED BY models.id;


--
-- TOC entry 198 (class 1259 OID 17052)
-- Name: modeltypesnames; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE modeltypesnames (
    pseudoname text NOT NULL,
    typename text
);


--
-- TOC entry 199 (class 1259 OID 17058)
-- Name: owners; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE owners (
    id integer NOT NULL,
    owner_name text,
    is_admin boolean,
    current_log text
);


--
-- TOC entry 200 (class 1259 OID 17064)
-- Name: plotdata; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE plotdata (
    id integer NOT NULL,
    model_id integer NOT NULL,
    model_view_item_pattern_id integer NOT NULL,
    results_id integer NOT NULL,
    xdata real[],
    ydata real[],
    is_standard boolean,
    labels text[]
);


--
-- TOC entry 201 (class 1259 OID 17070)
-- Name: plotdata_id_seq; Type: SEQUENCE; Schema: public; Owner: daisi_db_user
--

CREATE SEQUENCE plotdata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2391 (class 0 OID 0)
-- Dependencies: 201
-- Name: plotdata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: daisi_db_user
--

ALTER SEQUENCE plotdata_id_seq OWNED BY plotdata.id;


--
-- TOC entry 202 (class 1259 OID 17072)
-- Name: plottypesnames; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE plottypesnames (
    pseudoname text NOT NULL,
    typename text
);


--
-- TOC entry 203 (class 1259 OID 17078)
-- Name: results; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE results (
    id integer NOT NULL,
    model_id integer,
    real_model_id integer NOT NULL,
    component_name text NOT NULL,
    start_time timestamp without time zone,
    done_time timestamp without time zone,
    ready_status real,
    status text,
    name text,
    log text
);


--
-- TOC entry 204 (class 1259 OID 17084)
-- Name: results_id_seq; Type: SEQUENCE; Schema: public; Owner: daisi_db_user
--

CREATE SEQUENCE results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2392 (class 0 OID 0)
-- Dependencies: 204
-- Name: results_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: daisi_db_user
--

ALTER SEQUENCE results_id_seq OWNED BY results.id;


--
-- TOC entry 205 (class 1259 OID 17086)
-- Name: resultstates; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE resultstates (
    pseudoname text NOT NULL,
    name text
);


--
-- TOC entry 214 (class 1259 OID 18279)
-- Name: services; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE services (
    ip text NOT NULL,
    status text NOT NULL
);


--
-- TOC entry 216 (class 1259 OID 27490)
-- Name: textdata; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE textdata (
    id integer NOT NULL,
    model_id integer NOT NULL,
    model_view_item_pattern_id integer NOT NULL,
    results_id integer NOT NULL,
    data text
);


--
-- TOC entry 215 (class 1259 OID 27488)
-- Name: textdata_id_seq; Type: SEQUENCE; Schema: public; Owner: daisi_db_user
--

CREATE SEQUENCE textdata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2393 (class 0 OID 0)
-- Dependencies: 215
-- Name: textdata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: daisi_db_user
--

ALTER SEQUENCE textdata_id_seq OWNED BY textdata.id;


--
-- TOC entry 206 (class 1259 OID 17092)
-- Name: viewtypesnames; Type: TABLE; Schema: public; Owner: daisi_db_user
--

CREATE TABLE viewtypesnames (
    pseudoname text NOT NULL,
    typename text
);


--
-- TOC entry 2178 (class 2604 OID 17320)
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- TOC entry 2172 (class 2604 OID 17098)
-- Name: lazymodelcomponentsviewsitems id; Type: DEFAULT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY lazymodelcomponentsviewsitems ALTER COLUMN id SET DEFAULT nextval('lazymodelcomponentsviewsitems_id_seq'::regclass);


--
-- TOC entry 2173 (class 2604 OID 17099)
-- Name: modelcomponentsviewsplotitempattern id; Type: DEFAULT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewsplotitempattern ALTER COLUMN id SET DEFAULT nextval('modelcomponentsviewsplotitempattern_id_seq'::regclass);


--
-- TOC entry 2174 (class 2604 OID 17100)
-- Name: modelcomponentsviewssimpleitempattern id; Type: DEFAULT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewssimpleitempattern ALTER COLUMN id SET DEFAULT nextval('modelcomponentsviewssimpleitempattern_id_seq'::regclass);


--
-- TOC entry 2175 (class 2604 OID 17101)
-- Name: models id; Type: DEFAULT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY models ALTER COLUMN id SET DEFAULT nextval('models_id_seq'::regclass);


--
-- TOC entry 2176 (class 2604 OID 17102)
-- Name: plotdata id; Type: DEFAULT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY plotdata ALTER COLUMN id SET DEFAULT nextval('plotdata_id_seq'::regclass);


--
-- TOC entry 2177 (class 2604 OID 17103)
-- Name: results id; Type: DEFAULT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY results ALTER COLUMN id SET DEFAULT nextval('results_id_seq'::regclass);


--
-- TOC entry 2179 (class 2604 OID 27493)
-- Name: textdata id; Type: DEFAULT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY textdata ALTER COLUMN id SET DEFAULT nextval('textdata_id_seq'::regclass);


--
-- TOC entry 2225 (class 2606 OID 17325)
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2181 (class 2606 OID 17119)
-- Name: lazymodelcomponentsviewsitems lazymodelcomponentsviewsitems_id_key; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY lazymodelcomponentsviewsitems
    ADD CONSTRAINT lazymodelcomponentsviewsitems_id_key UNIQUE (id);


--
-- TOC entry 2188 (class 2606 OID 17121)
-- Name: modelcomponentstypesnames modelcomponentstypesnames_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentstypesnames
    ADD CONSTRAINT modelcomponentstypesnames_pkey PRIMARY KEY (pseudoname);


--
-- TOC entry 2190 (class 2606 OID 17123)
-- Name: modelcomponentsviewslabels modelcomponentsviewslabels_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewslabels
    ADD CONSTRAINT modelcomponentsviewslabels_pkey PRIMARY KEY (pseudoname);


--
-- TOC entry 2192 (class 2606 OID 17125)
-- Name: modelcomponentsviewsplotitempattern modelcomponentsviewsplotitempattern_id_key; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewsplotitempattern
    ADD CONSTRAINT modelcomponentsviewsplotitempattern_id_key UNIQUE (id);


--
-- TOC entry 2198 (class 2606 OID 17127)
-- Name: modelcomponentsviewssimpleitempattern modelcomponentsviewssimpleitempattern_id_key; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewssimpleitempattern
    ADD CONSTRAINT modelcomponentsviewssimpleitempattern_id_key UNIQUE (id);


--
-- TOC entry 2202 (class 2606 OID 17129)
-- Name: modelcomponentsviewssimpleitemtypenames modelcomponentsviewssimpleitemtypenames_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewssimpleitemtypenames
    ADD CONSTRAINT modelcomponentsviewssimpleitemtypenames_pkey PRIMARY KEY (pseudoname);


--
-- TOC entry 2204 (class 2606 OID 17131)
-- Name: models models_id_key; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY models
    ADD CONSTRAINT models_id_key UNIQUE (id);


--
-- TOC entry 2208 (class 2606 OID 17133)
-- Name: modeltypesnames modeltypesnames_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modeltypesnames
    ADD CONSTRAINT modeltypesnames_pkey PRIMARY KEY (pseudoname);


--
-- TOC entry 2210 (class 2606 OID 17135)
-- Name: owners owners_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY owners
    ADD CONSTRAINT owners_pkey PRIMARY KEY (id);


--
-- TOC entry 2212 (class 2606 OID 17137)
-- Name: plotdata pk_component_plot_data; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY plotdata
    ADD CONSTRAINT pk_component_plot_data PRIMARY KEY (model_id, model_view_item_pattern_id, results_id);


--
-- TOC entry 2183 (class 2606 OID 17308)
-- Name: lazymodelcomponentsviewsitems pk_lazy_model_components_views; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY lazymodelcomponentsviewsitems
    ADD CONSTRAINT pk_lazy_model_components_views PRIMARY KEY (model_id, model_view_item_pattern_id, component_name);


--
-- TOC entry 2186 (class 2606 OID 17281)
-- Name: modelcomponentsrelation pk_modelcomponentsrelation; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsrelation
    ADD CONSTRAINT pk_modelcomponentsrelation PRIMARY KEY (model_typename, component_typename, view_typename, name);


--
-- TOC entry 2206 (class 2606 OID 17141)
-- Name: models pk_models; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY models
    ADD CONSTRAINT pk_models PRIMARY KEY (label, typename, owner_name);


--
-- TOC entry 2200 (class 2606 OID 17145)
-- Name: modelcomponentsviewssimpleitempattern pk_models_components_view_pattenrs; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewssimpleitempattern
    ADD CONSTRAINT pk_models_components_view_pattenrs PRIMARY KEY (component_typename, item_label, view_label);


--
-- TOC entry 2194 (class 2606 OID 17147)
-- Name: modelcomponentsviewsplotitempattern pk_models_components_view_plot_patterns; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewsplotitempattern
    ADD CONSTRAINT pk_models_components_view_plot_patterns PRIMARY KEY (component_type, item_label);


--
-- TOC entry 2196 (class 2606 OID 17149)
-- Name: modelcomponentsviewsrelation pk_models_components_view_relations; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewsrelation
    ADD CONSTRAINT pk_models_components_view_relations PRIMARY KEY (component_type, label);


--
-- TOC entry 2229 (class 2606 OID 27498)
-- Name: textdata pk_text_data; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY textdata
    ADD CONSTRAINT pk_text_data PRIMARY KEY (model_id, model_view_item_pattern_id, results_id);


--
-- TOC entry 2214 (class 2606 OID 17151)
-- Name: plotdata plotdata_id_key; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY plotdata
    ADD CONSTRAINT plotdata_id_key UNIQUE (id);


--
-- TOC entry 2216 (class 2606 OID 17153)
-- Name: plottypesnames plottypesnames_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY plottypesnames
    ADD CONSTRAINT plottypesnames_pkey PRIMARY KEY (pseudoname);


--
-- TOC entry 2219 (class 2606 OID 17155)
-- Name: results results_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY results
    ADD CONSTRAINT results_pkey PRIMARY KEY (real_model_id, component_name);


--
-- TOC entry 2221 (class 2606 OID 17157)
-- Name: resultstates resultstates_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY resultstates
    ADD CONSTRAINT resultstates_pkey PRIMARY KEY (pseudoname);


--
-- TOC entry 2227 (class 2606 OID 18286)
-- Name: services services_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_pkey PRIMARY KEY (ip);


--
-- TOC entry 2231 (class 2606 OID 27500)
-- Name: textdata textdata_id_key; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY textdata
    ADD CONSTRAINT textdata_id_key UNIQUE (id);


--
-- TOC entry 2223 (class 2606 OID 17159)
-- Name: viewtypesnames viewtypesnames_pkey; Type: CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY viewtypesnames
    ADD CONSTRAINT viewtypesnames_pkey PRIMARY KEY (pseudoname);


--
-- TOC entry 2184 (class 1259 OID 17289)
-- Name: modelcomponentsrelation_name_key; Type: INDEX; Schema: public; Owner: daisi_db_user
--

CREATE UNIQUE INDEX modelcomponentsrelation_name_key ON public.modelcomponentsrelation USING btree (name);


--
-- TOC entry 2217 (class 1259 OID 27487)
-- Name: results_id_uindex; Type: INDEX; Schema: public; Owner: daisi_db_user
--

CREATE UNIQUE INDEX results_id_uindex ON public.results USING btree (id);


--
-- TOC entry 2234 (class 2606 OID 17290)
-- Name: lazymodelcomponentsviewsitems lazymodelcomponentsviewsitems_fk; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY lazymodelcomponentsviewsitems
    ADD CONSTRAINT lazymodelcomponentsviewsitems_fk FOREIGN KEY (component_name) REFERENCES modelcomponentsrelation(name);


--
-- TOC entry 2232 (class 2606 OID 17160)
-- Name: lazymodelcomponentsviewsitems lazymodelcomponentsviewsitems_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY lazymodelcomponentsviewsitems
    ADD CONSTRAINT lazymodelcomponentsviewsitems_model_id_fkey FOREIGN KEY (model_id) REFERENCES models(id);


--
-- TOC entry 2233 (class 2606 OID 17165)
-- Name: lazymodelcomponentsviewsitems lazymodelcomponentsviewsitems_model_view_item_pattern_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY lazymodelcomponentsviewsitems
    ADD CONSTRAINT lazymodelcomponentsviewsitems_model_view_item_pattern_id_fkey FOREIGN KEY (model_view_item_pattern_id) REFERENCES modelcomponentsviewssimpleitempattern(id);


--
-- TOC entry 2235 (class 2606 OID 17170)
-- Name: modelcomponentsrelation modelcomponentsrelation_component_typename_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsrelation
    ADD CONSTRAINT modelcomponentsrelation_component_typename_fkey FOREIGN KEY (component_typename) REFERENCES modelcomponentstypesnames(pseudoname);


--
-- TOC entry 2236 (class 2606 OID 17175)
-- Name: modelcomponentsrelation modelcomponentsrelation_model_typename_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsrelation
    ADD CONSTRAINT modelcomponentsrelation_model_typename_fkey FOREIGN KEY (model_typename) REFERENCES modeltypesnames(pseudoname);


--
-- TOC entry 2237 (class 2606 OID 17180)
-- Name: modelcomponentsrelation modelcomponentsrelation_view_typename_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsrelation
    ADD CONSTRAINT modelcomponentsrelation_view_typename_fkey FOREIGN KEY (view_typename) REFERENCES viewtypesnames(pseudoname);


--
-- TOC entry 2238 (class 2606 OID 17185)
-- Name: modelcomponentsviewsplotitempattern modelcomponentsviewsplotitempattern_component_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewsplotitempattern
    ADD CONSTRAINT modelcomponentsviewsplotitempattern_component_type_fkey FOREIGN KEY (component_type) REFERENCES modelcomponentstypesnames(pseudoname);


--
-- TOC entry 2239 (class 2606 OID 17190)
-- Name: modelcomponentsviewsplotitempattern modelcomponentsviewsplotitempattern_component_type_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewsplotitempattern
    ADD CONSTRAINT modelcomponentsviewsplotitempattern_component_type_fkey1 FOREIGN KEY (component_type, view_label) REFERENCES modelcomponentsviewsrelation(component_type, label);


--
-- TOC entry 2240 (class 2606 OID 17195)
-- Name: modelcomponentsviewsplotitempattern modelcomponentsviewsplotitempattern_format_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewsplotitempattern
    ADD CONSTRAINT modelcomponentsviewsplotitempattern_format_fkey FOREIGN KEY (format) REFERENCES plottypesnames(pseudoname);


--
-- TOC entry 2241 (class 2606 OID 17200)
-- Name: modelcomponentsviewsplotitempattern modelcomponentsviewsplotitempattern_view_label_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewsplotitempattern
    ADD CONSTRAINT modelcomponentsviewsplotitempattern_view_label_fkey FOREIGN KEY (view_label) REFERENCES modelcomponentsviewslabels(pseudoname);


--
-- TOC entry 2242 (class 2606 OID 17205)
-- Name: modelcomponentsviewsrelation modelcomponentsviewsrelation_component_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewsrelation
    ADD CONSTRAINT modelcomponentsviewsrelation_component_type_fkey FOREIGN KEY (component_type) REFERENCES modelcomponentstypesnames(pseudoname);


--
-- TOC entry 2243 (class 2606 OID 17210)
-- Name: modelcomponentsviewsrelation modelcomponentsviewsrelation_label_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewsrelation
    ADD CONSTRAINT modelcomponentsviewsrelation_label_fkey FOREIGN KEY (label) REFERENCES modelcomponentsviewslabels(pseudoname);


--
-- TOC entry 2244 (class 2606 OID 17215)
-- Name: modelcomponentsviewssimpleitempattern modelcomponentsviewssimpleitempattern_component_typename_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewssimpleitempattern
    ADD CONSTRAINT modelcomponentsviewssimpleitempattern_component_typename_fkey FOREIGN KEY (component_typename) REFERENCES modelcomponentstypesnames(pseudoname);


--
-- TOC entry 2245 (class 2606 OID 17220)
-- Name: modelcomponentsviewssimpleitempattern modelcomponentsviewssimpleitempattern_component_typename_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewssimpleitempattern
    ADD CONSTRAINT modelcomponentsviewssimpleitempattern_component_typename_fkey1 FOREIGN KEY (component_typename, view_label) REFERENCES modelcomponentsviewsrelation(component_type, label);


--
-- TOC entry 2246 (class 2606 OID 17225)
-- Name: modelcomponentsviewssimpleitempattern modelcomponentsviewssimpleitempattern_item_typename_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewssimpleitempattern
    ADD CONSTRAINT modelcomponentsviewssimpleitempattern_item_typename_fkey FOREIGN KEY (item_typename) REFERENCES modelcomponentsviewssimpleitemtypenames(pseudoname);


--
-- TOC entry 2247 (class 2606 OID 17230)
-- Name: modelcomponentsviewssimpleitempattern modelcomponentsviewssimpleitempattern_view_label_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY modelcomponentsviewssimpleitempattern
    ADD CONSTRAINT modelcomponentsviewssimpleitempattern_view_label_fkey FOREIGN KEY (view_label) REFERENCES modelcomponentsviewslabels(pseudoname);


--
-- TOC entry 2248 (class 2606 OID 17235)
-- Name: models models_lock_owner_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY models
    ADD CONSTRAINT models_lock_owner_name_fkey FOREIGN KEY (lock_owner_name) REFERENCES owners(id);


--
-- TOC entry 2249 (class 2606 OID 17240)
-- Name: models models_owner_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY models
    ADD CONSTRAINT models_owner_name_fkey FOREIGN KEY (owner_name) REFERENCES owners(id);


--
-- TOC entry 2250 (class 2606 OID 17245)
-- Name: models models_typename_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY models
    ADD CONSTRAINT models_typename_fkey FOREIGN KEY (typename) REFERENCES modeltypesnames(pseudoname);


--
-- TOC entry 2251 (class 2606 OID 17250)
-- Name: plotdata plotdata_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY plotdata
    ADD CONSTRAINT plotdata_model_id_fkey FOREIGN KEY (model_id) REFERENCES models(id);


--
-- TOC entry 2252 (class 2606 OID 17255)
-- Name: plotdata plotdata_model_view_item_pattern_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY plotdata
    ADD CONSTRAINT plotdata_model_view_item_pattern_id_fkey FOREIGN KEY (model_view_item_pattern_id) REFERENCES modelcomponentsviewsplotitempattern(id);


--
-- TOC entry 2253 (class 2606 OID 17260)
-- Name: results results_component_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY results
    ADD CONSTRAINT results_component_name_fkey FOREIGN KEY (component_name) REFERENCES modelcomponentstypesnames(pseudoname);


--
-- TOC entry 2257 (class 2606 OID 17309)
-- Name: results results_fk; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY results
    ADD CONSTRAINT results_fk FOREIGN KEY (name) REFERENCES modelcomponentsrelation(name);


--
-- TOC entry 2254 (class 2606 OID 17265)
-- Name: results results_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY results
    ADD CONSTRAINT results_model_id_fkey FOREIGN KEY (model_id) REFERENCES models(id);


--
-- TOC entry 2255 (class 2606 OID 17270)
-- Name: results results_real_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY results
    ADD CONSTRAINT results_real_model_id_fkey FOREIGN KEY (real_model_id) REFERENCES models(id);


--
-- TOC entry 2256 (class 2606 OID 17275)
-- Name: results results_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY results
    ADD CONSTRAINT results_status_fkey FOREIGN KEY (status) REFERENCES resultstates(pseudoname);


--
-- TOC entry 2258 (class 2606 OID 27501)
-- Name: textdata textdata_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY textdata
    ADD CONSTRAINT textdata_model_id_fkey FOREIGN KEY (model_id) REFERENCES models(id);


--
-- TOC entry 2259 (class 2606 OID 27506)
-- Name: textdata textdata_model_view_item_pattern_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY textdata
    ADD CONSTRAINT textdata_model_view_item_pattern_id_fkey FOREIGN KEY (model_view_item_pattern_id) REFERENCES modelcomponentsviewssimpleitempattern(id);


--
-- TOC entry 2260 (class 2606 OID 27511)
-- Name: textdata textdata_results_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: daisi_db_user
--

ALTER TABLE ONLY textdata
    ADD CONSTRAINT textdata_results_id_fkey FOREIGN KEY (results_id) REFERENCES results(id);


-- Completed on 2020-02-08 11:25:12

--
-- PostgreSQL database dump complete
--

GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO daisi_db_user;