#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
MAGENTA='\033[0;35m'
CYAN='\033[0;36m'
WHITE='\033[0;97m'
NORMAL='\033[0m'

CUR_DIR="/opt/daisi/db"
PSQL_USER="postgres"

DB="daisi_db"
PSQL="psql"
CREATE_DB="createdb"
DROP_DB="dropdb"

ERR_LOG="/opt/daisi/log/install_db_err.log"
WRITE_ERR_LOG="2>>${ERR_LOG}"

ACTION=

function get_input_parameters ()
{
    if [[ "$1" == "-h" || "$1" == "--help" || "$1" == "help" ]]; then
        echo "Usage: deploy_db.sh <action>"
        echo "  action can be:"
		echo "    - create - create database $DB from scratch"
        echo "    - update - update database schema if necessary"
        exit 0
    fi

    ACTION=${1:-create}
}

function deploy_sql( )
{
    local __SQL_FILE="${1}"
    [ ! -e ${__SQL_FILE} ] && return 1
    ${DBG_TEST} su - ${PSQL_USER} -c "${PSQL} -d ${DB} -f ${__SQL_FILE} ${WRITE_ERR_LOG}"
}

function deploy_sql_nolog( )
{
    local __SQL_FILE="${1}"
    [ ! -e ${__SQL_FILE} ] && return 1
    ${DBG_TEST} su - ${PSQL_USER} -c "${PSQL} -d ${DB} -f ${__SQL_FILE}"
}

function exec_cmd( )
{
    local __CMD="${1}"
    ${DBG_TEST} su - ${PSQL_USER} -c "${PSQL} -d ${DB} -c '${__CMD}' ${WRITE_ERR_LOG}"
}

function exec_cmd_nolog( )
{
    local __CMD="${1}"
    ${DBG_TEST} su - ${PSQL_USER} -c "${PSQL} -d ${DB} -c '${__CMD}' 1>/dev/null"
}

OBJECT_SYSTEM=17179869184
EVENT_DB_UPDATE_START=12884902147
EVENT_DB_UPDATE_SUCCESS=12884902148
EVENT_DB_UPDATE_FAIL=12884902149
function system_event( )
{
    local event_type="${1}"
    local object="${2}"
    local arg1="${3}"
    local arg2="${4}"
	
	if test -z "$arg1"; then
		exec_cmd_nolog "select register_system_event($event_type,$object)"
	elif test -z "$arg2"; then
		exec_cmd_nolog "select register_system_event($event_type,$object,array[$arg1]::text[])"
	else
		exec_cmd_nolog "select register_system_event($event_type,$object,array[$arg1,$arg2]::text[])"
	fi
}

function create_database()
{
	echo -e "${GREEN}Creating database${NORMAL}"
	rm -f ${ERR_LOG}

        echo -e "${GREEN}${CUR_DIR}${NORMAL}"

    # drop DB:
    ${DBG_TEST} su - ${PSQL_USER} -c "${DROP_DB} --if-exists ${DB} ${WRITE_ERR_LOG}"

    # creating DB:
    ${DBG_TEST} su - ${PSQL_USER} -c "${CREATE_DB} ${DB} 2>${ERR_LOG}"

    # user
    deploy_sql "${CUR_DIR}/user.sql"

    # creating
    deploy_sql "${CUR_DIR}/daisi_schema.sql"

    # fucntions
    deploy_sql "${CUR_DIR}/daisi_functions.sql"

    # data
    zcat "${CUR_DIR}/daisi_data.gzip" > "${CUR_DIR}/daisi_data.gzip.sql"
    deploy_sql "${CUR_DIR}/daisi_data.gzip.sql"
    rm ${CUR_DIR}/daisi_data.gzip.sql
	
    TARGET_VERSION=`cat "${CUR_DIR}/schema_version.txt"`
	#TARGET_VERSION=$(($TARGET_VERSION+0))
	
	# set version
    ${DBG_TEST} su - ${PSQL_USER} -c "${PSQL} -d ${DB} -qtc \"select set_version($TARGET_VERSION)\" 1>/dev/null"
	
    if [ -s ${ERR_LOG} ]
    then
        # При развертывании БД возникла ошибка
        ${DBG_TEST} su - ${PSQL_USER} -c "${DROP_DB} ${DB} ${WRITE_ERR_LOG}"
        echo -e "\n\n${RED}An error occurred while deploying the database${NORMAL}\n\n"
        sleep 5s
        return 1
    fi
}

function update_database()
{
	CURRENT_VERSION=`su - ${PSQL_USER} -c "${PSQL} -d ${DB} -qtc \"select get_version()\""`
	#CURRENT_VERSION=$(($CURRENT_VERSION+0))
	TARGET_VERSION=`cat ${CUR_DIR}/schema_version.txt`
	TARGET_VERSION=$(($TARGET_VERSION+0))
    echo -e "${GREEN}Updating database from v $CURRENT_VERSION to v $TARGET_VERSION${NORMAL}"
	if [[ $TARGET_VERSION -gt $CURRENT_VERSION ]]; then
	
		for v in `seq $(($CURRENT_VERSION+1)) $TARGET_VERSION`; do
			echo -e "  ${GREEN}$(($v-1))${NORMAL} -> ${GREEN}$v${NORMAL} (${CUR_DIR}/updates/v$v/.sql)"
			deploy_sql "${CUR_DIR}/updates/v$v.sql"
			
			if [ -s ${ERR_LOG} ] 
            then
				echo -e "${RED}  error while applying patch ${NORMAL}"			
				sleep 1s
				return 1
			fi
			
			${DBG_TEST} su - ${PSQL_USER} -c "${PSQL} -d ${DB} -qtc \"select set_version($v)\" 1>/dev/null"
		done
	else
            echo -e "${GREEN}target version <= than current${NORMAL}"
	fi
	
	return 0
}

get_input_parameters $@
case ${ACTION} in
    [cC]*)
		create_database
    ;;
    [uU]*)
        update_database
    ;;
    *)
        echo "Bad input parameter! Use -h for help." && exit 1
    ;;
esac

exit 0
