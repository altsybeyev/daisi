--
-- TOC entry 270 (class 1255 OID 17314)
-- Name: add_results(integer, text, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION add_results(model_id_in integer, component_name_in text, name_in text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE plot_view_id INTEGER; text_view_id INTEGER; new_id INTEGER; new_id_plot INTEGER; result Results%ROWTYPE;
		BEGIN
        new_id = get_new_id('Results');
        result.model_id = model_id_in;
        result.real_model_id = model_id_in;
        result.component_name = component_name_in;
        result.name = name_in;
        result.id = new_id;
        result.start_time = NOW();
        result.done_time = NULL;
        result.ready_status = 0;
        result.status = 'In_queue';
        INSERT INTO Results VALUES(result.*);

        FOR plot_view_id IN SELECT id FROM ModelComponentsViewsPlotItemPattern WHERE component_type=component_name_in
        LOOP
         -- raise notice 'model_id_in = % new_id id = % new id = %', model_id_in, plot_view_id, new_id;
           new_id_plot  = get_new_id('PlotData');
        	INSERT INTO PlotData(id, model_id, model_view_item_pattern_id, results_id) VALUES(new_id_plot, model_id_in, plot_view_id, new_id);
        END LOOP;
        FOR text_view_id IN SELECT id FROM modelcomponentsviewssimpleitempattern WHERE component_typename=component_name_in AND is_result = True
        LOOP
        	INSERT INTO TextData(model_id, model_view_item_pattern_id, results_id) VALUES(model_id_in, text_view_id, new_id);
        END LOOP;

        return new_id;
        END;
$$;


--
-- TOC entry 263 (class 1255 OID 27319)
-- Name: add_solver(text, text, text, text, text, text[], boolean, boolean); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION add_solver(_component_type text, _item_label text, _format text, _xlabel text, _ylabel text, _labels text[], _is_editable boolean, _is_result boolean) RETURNS void
    LANGUAGE sql
    AS $$
INSERT INTO modelcomponentsviewsplotitempattern(component_type, view_label, item_label, format, xlabel, ylabel, labels, is_editable, is_result) 
VALUES(_component_type, '', _item_label, _format, _xlabel, _ylabel, _labels, _is_editable, _is_result);
 $$;


--
-- TOC entry 219 (class 1255 OID 16955)
-- Name: check_lazy_item_data(integer, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION check_lazy_item_data(item_id_in integer, content_in text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    DECLARE old_content TEXT;
    BEGIN 
        SELECT INTO old_content content FROM LazyModelComponentsViewsItems WHERE id = item_id_in;
      IF old_content=content_in THEN
        return false;
      END IF;
      return true;
    END;
$$;


--
-- TOC entry 255 (class 1255 OID 18226)
-- Name: clear_current_log_data(text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION clear_current_log_data(user_name text) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE owners SET current_log = '' WHERE owner_name = user_name
$$;


--
-- TOC entry 234 (class 1255 OID 16956)
-- Name: copy_model(integer, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION copy_model(copied_id integer, new_label text) RETURNS void
    LANGUAGE plpgsql
    AS $$
        DECLARE  copied Models%ROWTYPE; view_item LazyModelComponentsViewsItems%ROWTYPE; plot_view_item PlotData%ROWTYPE;
        BEGIN
        SELECT INTO copied * FROM Models WHERE id = copied_id;
        copied.label = new_label;
        copied.id = get_new_id('Models');
 		INSERT INTO Models VALUES(copied.*);
        
          FOR view_item IN SELECT * FROM LazyModelComponentsViewsItems WHERE model_id = copied_id
          LOOP
              view_item.id = get_new_id('LazyModelComponentsViewsItems');
              view_item.model_id = copied.id;
              INSERT INTO LazyModelComponentsViewsItems VALUES(view_item.*);
          END LOOP;
          
          FOR plot_view_item IN SELECT * FROM PlotData WHERE model_id = copied_id AND model_view_item_pattern_id != -1
          LOOP
              plot_view_item.id = get_new_id('PlotData');
              plot_view_item.model_id = copied.id;
              INSERT INTO PlotData VALUES(plot_view_item.*);
          END LOOP;

        END;
$$;


--
-- TOC entry 235 (class 1255 OID 16957)
-- Name: copy_model(integer, text, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION copy_model(copied_id integer, new_label text, new_description text) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE  copied Models%ROWTYPE; view_item LazyModelComponentsViewsItems%ROWTYPE; plot_view_item PlotData%ROWTYPE;
        BEGIN
        SELECT INTO copied * FROM Models WHERE id = copied_id;
        copied.label = new_label;
        copied.description = new_description;
        copied.id = get_new_id('Models');
        copied.created = NOW();
        copied.edited = NOW();
 		INSERT INTO Models VALUES(copied.*);
        
          FOR view_item IN SELECT * FROM LazyModelComponentsViewsItems WHERE model_id = copied_id
          LOOP
              view_item.id = get_new_id('LazyModelComponentsViewsItems');
              view_item.model_id = copied.id;
              INSERT INTO LazyModelComponentsViewsItems VALUES(view_item.*);
          END LOOP;
          
          FOR plot_view_item IN SELECT * FROM PlotData WHERE model_id = copied_id AND model_view_item_pattern_id != -1
          LOOP
              plot_view_item.id = get_new_id('PlotData');
              plot_view_item.model_id = copied.id;
              INSERT INTO PlotData VALUES(plot_view_item.*);
          END LOOP;

        END;
$$;


--
-- TOC entry 236 (class 1255 OID 16958)
-- Name: copy_model(integer, integer, text, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION copy_model(copied_id integer, owner_id integer, new_label text, new_description text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE  copied Models%ROWTYPE; view_item LazyModelComponentsViewsItems%ROWTYPE; plot_view_item PlotData%ROWTYPE;
        BEGIN
        SELECT INTO copied * FROM Models WHERE id = copied_id;
        copied.label = new_label;
        copied.description = new_description;
        copied.id = get_new_id('Models');
        copied.created = NOW();
        copied.edited = NOW();
        copied.owner_name = owner_id;
 		INSERT INTO Models VALUES(copied.*);
        
          FOR view_item IN SELECT * FROM LazyModelComponentsViewsItems WHERE model_id = copied_id
          LOOP
              view_item.id = get_new_id('LazyModelComponentsViewsItems');
              view_item.model_id = copied.id;
              INSERT INTO LazyModelComponentsViewsItems VALUES(view_item.*);
          END LOOP;
          
          FOR plot_view_item IN SELECT * FROM PlotData WHERE model_id = copied_id AND model_view_item_pattern_id != -1
          LOOP
              plot_view_item.id = get_new_id('PlotData');
              plot_view_item.model_id = copied.id;
              INSERT INTO PlotData VALUES(plot_view_item.*);
          END LOOP;
    return copied.id;    

    END;
$$;


--
-- TOC entry 264 (class 1255 OID 16959)
-- Name: copy_model(integer, integer, text, text, boolean); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION copy_model(copied_id integer, owner_id integer, new_label text, new_description text, clone_results boolean) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE  copied Models%ROWTYPE; view_item LazyModelComponentsViewsItems%ROWTYPE; plot_view_item PlotData%ROWTYPE; is_result_ BOOL;
        BEGIN
        SELECT INTO copied * FROM Models WHERE id = copied_id;
        copied.label = new_label;
        copied.description = new_description;
        copied.id = get_new_id('Models');
        copied.clone_source = copied.id;
        copied.created = NOW();
        copied.edited = NOW();
        copied.owner_name = owner_id;
        copied.is_hidden = clone_results;
 		INSERT INTO Models VALUES(copied.*);
--   PERFORM  copy_results(copied_id, copied.id);
        
    FOR view_item IN SELECT * FROM LazyModelComponentsViewsItems WHERE model_id = copied_id
    LOOP
        SELECT INTO is_result_ is_result FROM modelcomponentsviewssimpleitempattern WHERE id = view_item.model_view_item_pattern_id;

      IF is_result_=FALSE THEN
        view_item.id = get_new_id('LazyModelComponentsViewsItems');
        view_item.model_id = copied.id;
        INSERT INTO LazyModelComponentsViewsItems VALUES(view_item.*);
       END IF;
    END LOOP;
          
    return copied.id;    

    END;
$$;


--
-- TOC entry 265 (class 1255 OID 27376)
-- Name: copy_model(integer, integer, text, text, boolean, boolean); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION copy_model(copied_id integer, owner_id integer, new_label text, new_description text, is_hidden boolean, clone_results boolean) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE  copied Models%ROWTYPE; view_item LazyModelComponentsViewsItems%ROWTYPE; plot_view_item PlotData%ROWTYPE; is_result_ BOOL;
        BEGIN
        SELECT INTO copied * FROM Models WHERE id = copied_id;
        copied.label = new_label;
        copied.description = new_description;
        copied.id = get_new_id('Models');
        copied.clone_source = copied.id;
        copied.created = NOW();
        copied.edited = NOW();
        copied.owner_name = owner_id;
        copied.is_hidden = is_hidden;
 		INSERT INTO Models VALUES(copied.*);
  if clone_results=true THEN
  PERFORM  copy_results(copied_id, copied.id);
  end if ;
        
    FOR view_item IN SELECT * FROM LazyModelComponentsViewsItems WHERE model_id = copied_id
    LOOP
        SELECT INTO is_result_ is_result FROM modelcomponentsviewssimpleitempattern WHERE id = view_item.model_view_item_pattern_id;

      IF is_result_=FALSE THEN
        view_item.id = get_new_id('LazyModelComponentsViewsItems');
        view_item.model_id = copied.id;
        INSERT INTO LazyModelComponentsViewsItems VALUES(view_item.*);
       END IF;
    END LOOP;
          
    return copied.id;    

    END;
$$;


--
-- TOC entry 268 (class 1255 OID 16960)
-- Name: copy_model_results(integer, boolean, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION copy_model_results(copied_id integer, is_result_flag boolean, component text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE results_ids text; old_label text; is_result_ BOOL; new_description text; owner_id integer; results_id integer; new_id INTEGER; view_item LazyModelComponentsViewsItems%ROWTYPE;
BEGIN
  RAISE NOTICE 'begin';
  results_ids = '';

  IF is_result_flag = false THEN
    FOR results_id IN SELECT id FROM Results WHERE real_model_id = copied_id
    LOOP
        results_ids:=results_ids || to_char(results_id, '999');
    END LOOP;
  END IF;

  IF is_result_flag = true THEN
    FOR results_id IN SELECT id FROM Results WHERE real_model_id = copied_id AND component_name = component
    LOOP
        results_ids:=results_ids || to_char(results_id, '999');
    END LOOP;
  END IF;


  IF results_ids = '' THEN
  	return -1;
  END IF;
  RAISE NOTICE 'results_ids = %', results_ids;

  SELECT INTO old_label label FROM Models WHERE id = copied_id;
  SELECT INTO owner_id owner_name FROM Models WHERE id = copied_id;

  old_label:=old_label || results_ids;
  new_description:='Clone of model with id = ' || to_char(copied_id, '999') || ', related with result ids: ' || results_ids;
  new_id = copy_model(copied_id, owner_id, old_label, new_description, true, false);
  RAISE NOTICE 'new_id = %', new_id;

  UPDATE Models 
    SET clone_source = copied_id  
  WHERE id = new_id;  
    
  IF is_result_flag = false THEN  
    UPDATE Results 
    SET real_model_id = new_id
    WHERE model_id = copied_id AND real_model_id = copied_id;
      
    UPDATE plotdata 
    SET model_id = new_id
    WHERE model_id = copied_id; 
  END IF;
    
  IF is_result_flag = true THEN  
    UPDATE Results 
    SET real_model_id = new_id
    WHERE model_id = copied_id AND real_model_id = copied_id AND component_name = component;
    
    UPDATE plotdata 
    SET model_id = new_id
    WHERE id = ANY(SELECT id FROM Results WHERE model_id = copied_id AND real_model_id = copied_id AND component_name = component); 
  END IF;

    
  FOR view_item IN SELECT * FROM LazyModelComponentsViewsItems WHERE model_id = copied_id
    LOOP
      SELECT INTO is_result_ is_result FROM modelcomponentsviewssimpleitempattern WHERE id = view_item.model_view_item_pattern_id;
        
      IF is_result_=TRUE THEN
          UPDATE LazyModelComponentsViewsItems 
          SET model_id = new_id
          WHERE id = view_item.id;
      END IF;
  END LOOP;   
    
    
    
  return new_id;  

    
END;
$$;


--
-- TOC entry 261 (class 1255 OID 23590)
-- Name: copy_results(integer); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION copy_results(_model_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE  copied_result Results%ROWTYPE; copied_plot PlotData%ROWTYPE; new_id int4;
  BEGIN

          FOR copied_result IN SELECT * FROM results WHERE model_id = _model_id
          LOOP
              new_id = get_new_id('Results');
            
              FOR copied_plot IN SELECT * FROM PlotData WHERE model_id = _model_id AND results_id = copied_result.id
              LOOP
                   copied_plot.id=get_new_id('PlotData');
                  INSERT INTO PlotData VALUES(copied_plot.*);
              END LOOP;
            
              copied_result.id = new_id;
              INSERT INTO results VALUES(copied_result.*);
          END LOOP;
          
    return copied.id;    

    END;
$$;


--
-- TOC entry 266 (class 1255 OID 23591)
-- Name: copy_results(integer, integer); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION copy_results(_model_id integer, new_model_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE  copied_result Results%ROWTYPE; copied_plot PlotData%ROWTYPE; new_id int4;
  BEGIN

          FOR copied_result IN SELECT * FROM results WHERE model_id = _model_id
          LOOP
              new_id = get_new_id('Results');

              FOR copied_plot IN SELECT * FROM PlotData WHERE model_id = _model_id AND results_id = copied_result.id
              LOOP
                   copied_plot.id=get_new_id('PlotData');
                   copied_plot.results_id=new_id;
                   copied_plot.model_id = new_model_id;

                   INSERT INTO PlotData VALUES(copied_plot.*);
              END LOOP;

              copied_result.id = new_id;
              copied_result.model_id = new_model_id;
              copied_result.real_model_id = new_model_id;

              INSERT INTO results VALUES(copied_result.*);
          END LOOP;

    return 0;    

    END;
$$;


--
-- TOC entry 237 (class 1255 OID 16961)
-- Name: delete_model(integer); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION delete_model(model_id_in integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE cur_id integer; 
  BEGIN
 
    
        FOR cur_id IN SELECT id FROM Models WHERE clone_source = model_id_in OR id=model_id_in
        LOOP
           PERFORM delete_model_internal(cur_id);
        END LOOP;
END;
$$;


--
-- TOC entry 240 (class 1255 OID 16962)
-- Name: delete_model_internal(integer); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION delete_model_internal(model_id_in integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
  results_id integer;
BEGIN
    
  
DELETE FROM LazyModelComponentsViewsItems WHERE model_id = model_id_in;
DELETE FROM PlotData WHERE model_id = model_id_in;

FOR results_id IN SELECT id FROM Results WHERE model_id = model_id_in OR real_model_id = model_id_in
LOOP
  perform delete_results(results_id);
END LOOP;

DELETE FROM Models WHERE id = model_id_in;

END;
$$;


--
-- TOC entry 269 (class 1255 OID 16963)
-- Name: delete_results(integer); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION delete_results(results_id_in integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE real_model_id_ INT; model_id_ INT; n_res INT;n_r_models INT;
  BEGIN
    
SELECT INTO real_model_id_ real_model_id FROM Results WHERE id = results_id_in;
SELECT INTO model_id_ model_id FROM Results WHERE id = results_id_in;
      
      
DELETE FROM 
  lazymodelcomponentsviewsitems
WHERE model_view_item_pattern_id = 
      ANY(
        SELECT id FROM modelcomponentsviewssimpleitempattern WHERE component_typename = (SELECT component_name FROM results WHERE id = results_id_in) AND is_result = true
      ) AND model_id = real_model_id_;      
      
--IF model_id_!=real_model_id_ THEN
--  SELECT INTO n_res COUNT(*) FROM Results WHERE real_model_id = real_model_id_;
--  IF n_res=1 THEN
--    PERFORM (SELECT * FROM delete_model(real_model_id_));
--  END IF;
--END IF;
      
    
DELETE FROM PlotData WHERE results_id = results_id_in;
DELETE FROM TextData WHERE results_id = results_id_in;

DELETE FROM Results WHERE id = results_id_in;

SELECT into n_r_models COUNT(*) FROM Results WHERE real_model_id = real_model_id_;

  if n_r_models =0 and real_model_id_!=model_id_ then
    perform delete_model(real_model_id_);
  end if;

END;
$$;

CREATE OR REPLACE FUNCTION edit_model(model_id_in integer, new_label text, new_description text) RETURNS void
    LANGUAGE sql
    AS $$
UPDATE Models
SET label = new_label, description = new_description
WHERE id = model_id_in
$$;

CREATE OR REPLACE FUNCTION get_model_components_ids(model_id integer, view_type_id integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$  << outerblock >>
		BEGIN
		RETURN QUERY SELECT component_type FROM ModelComponentsRelation WHERE view_type=view_type_id AND model_type = (SELECT model_type FROM Models WHERE id = model_id);
        RETURN;
        END; $$;


CREATE OR REPLACE FUNCTION get_model_components_labels(model_id integer, view_type_id integer) RETURNS TABLE(labels text)
    LANGUAGE plpgsql
    AS $$  << outerblock >>
		BEGIN
		RETURN QUERY SELECT data_type FROM ModelComponentsTypes WHERE id IN (SELECT get_model_components_ids(model_id, view_type_id));
        END; $$;


CREATE OR REPLACE FUNCTION get_model_components_views(model_id_in integer) RETURNS SETOF return_type2
    LANGUAGE sql
    AS $$
SELECT
  component_type,
  label,
  name
FROM
  (
    SELECT
      ModelComponentsViewsRelation.component_type,
      ModelComponentsViewsRelation.label,
      modelcomponentsrelation.name,
      rank()
     OVER (PARTITION BY component_type ORDER BY label DESC)
    FROM
      ModelComponentsViewsRelation
      INNER JOIN modelcomponentsrelation
        ON ModelComponentsViewsRelation.component_type = modelcomponentsrelation.component_typename
    WHERE ModelComponentsViewsRelation.component_type = ANY
          (
            SELECT component_typename
            FROM
              ModelComponentsRelation
            WHERE
              model_typename =
              (
                SELECT typename
                FROM
                  Models
                WHERE
                  id = model_id_in
              )
              AND
              view_typename = 'Model'
          )
          AND
          is_editable = TRUE
  ) t
      WHERE
  t.rank < 2
$$;


CREATE OR REPLACE FUNCTION get_model_components_views_json(model_id_in integer) RETURNS SETOF json
    LANGUAGE sql
    AS $$
        SELECT row_to_json(t)
        FROM (
           SELECT * FROM get_model_components_views(model_id_in)
        ) t
        $$;


CREATE OR REPLACE FUNCTION get_model_view_item_id(model_id_in integer, model_view_item_pattern_id_in integer, name text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
        DECLARE default_content_loc text;
        BEGIN
            	SELECT INTO default_content_loc default_content from ModelComponentsViewsSimpleItemPattern WHERE id = model_view_item_pattern_id_in;
        RETURN get_model_view_item_id_inner(model_id_in, model_view_item_pattern_id_in, default_content_loc, name);
        END;
$$;

CREATE OR REPLACE FUNCTION get_model_view_item_id_inner(model_id_in integer, model_view_item_pattern_id_in integer, default_content text, component_name_in text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
        DECLARE current_content text; result_id integer;
        BEGIN
		            SELECT INTO result_id id from LazyModelComponentsViewsItems
                WHERE model_id = model_id_in AND model_view_item_pattern_id = model_view_item_pattern_id_in AND component_name=component_name_in;
	            	IF result_id IS NULL THEN
                        current_content = default_content;
                                INSERT INTO LazyModelComponentsViewsItems(model_id, model_view_item_pattern_id, content, is_valid, component_name)
                                VALUES(model_id_in, model_view_item_pattern_id_in, current_content, true, component_name_in) RETURNING id INTO result_id;
                END IF;
                RETURN result_id;
        END;
$$;


--
-- TOC entry 248 (class 1255 OID 17299)
-- Name: get_model_view_items_ids(integer, text, text, boolean, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION get_model_view_items_ids(model_id_in integer, model_component_pseudoname text, view_label_pseudoname text, is_result_in boolean, component_name text) RETURNS integer[]
    LANGUAGE plpgsql
    AS $$
        DECLARE model_view_item_pattern_id integer; result INTEGER[]; i INTEGER;
        BEGIN
        i := 0;
        FOR model_view_item_pattern_id IN SELECT id FROM ModelComponentsViewsSimpleItemPattern
        WHERE component_typename = model_component_pseudoname AND view_label = view_label_pseudoname AND is_result = is_result_in
        LOOP
              i := i+1;
              result[i] := get_model_view_item_id(model_id_in, model_view_item_pattern_id, component_name);
        END LOOP;
        return result;
        END;
$$;


--
-- TOC entry 249 (class 1255 OID 17300)
-- Name: get_model_view_items(integer, text, text, boolean, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION get_model_view_items(model_id_in integer, model_component_pseudoname text, view_label_pseudoname text, is_result_in boolean, component_name text) RETURNS SETOF lazymodelcomponentsviewsitems
    LANGUAGE sql
    AS $$
  SELECT * FROM LazyModelComponentsViewsItems
        WHERE id = ANY((SELECT get_model_view_items_ids(model_id_in, model_component_pseudoname, view_label_pseudoname, is_result_in, component_name))::INT[])
$$;



--
-- TOC entry 267 (class 1255 OID 17304)
-- Name: get_model_view_items_join(integer, text, text, boolean, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION get_model_view_items_join(model_id_in integer, model_component_pseudoname text, view_label_pseudoname text, is_result_in boolean, component_name_in text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
  if is_result_in = FALSE then
          PERFORM get_model_view_items_ids(model_id_in, model_component_pseudoname,view_label_pseudoname, is_result_in, component_name_in);
          RETURN QUERY SELECT LazyModelComponentsViewsItems.id, LazyModelComponentsViewsItems.is_valid, LazyModelComponentsViewsItems.content, ModelComponentsViewsSimpleItemPattern.item_typename,  ModelComponentsViewsSimpleItemPattern.item_label FROM LazyModelComponentsViewsItems INNER JOIN
          ModelComponentsViewsSimpleItemPattern
          ON LazyModelComponentsViewsItems.model_view_item_pattern_id = ModelComponentsViewsSimpleItemPattern.id
          WHERE LazyModelComponentsViewsItems.id = ANY((SELECT get_model_view_items_ids(model_id_in, model_component_pseudoname, view_label_pseudoname, is_result_in, component_name_in))::INT[]);
  else

  end if;

  RETURN;
  END;
$$;


--
-- TOC entry 251 (class 1255 OID 17302)
-- Name: get_model_view_items_join_json(integer, text, text, boolean, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION get_model_view_items_join_json(model_id_in integer, model_component_pseudoname text, view_label_pseudoname text, is_result_in boolean, component_name text) RETURNS SETOF json
    LANGUAGE sql
    AS $$
  SELECT row_to_json(t)
        FROM (
           SELECT * FROM get_model_view_items_join(model_id_in, model_component_pseudoname, view_label_pseudoname, is_result_in, component_name) AS
             f(id INT, is_valid bool, content text, item_typename text, item_label text)
        ) t
$$;


--
-- TOC entry 241 (class 1255 OID 16985)
-- Name: get_new_id(character varying); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION get_new_id(table_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$  << outerblock >>
		DECLARE new_id integer;
        BEGIN
        EXECUTE 'SELECT MAX(id) from ' ||  table_name INTO new_id;
        IF new_id IS NULL THEN
        	new_id = -1;
        END IF;
        new_id = new_id + 1;
        RETURN new_id;
        END; $$;



--
-- TOC entry 220 (class 1255 OID 18311)
-- Name: get_services_list(); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION get_services_list() RETURNS SETOF json
    LANGUAGE sql
    AS $$
  SELECT row_to_json(t)
        FROM (
           SELECT * FROM services
        ) t
$$;


--
-- TOC entry 221 (class 1255 OID 17331)
-- Name: get_tasks(); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION get_tasks() RETURNS SETOF task_data
    LANGUAGE sql
    AS $$
  SELECT Results.id, Results.real_model_id, Results.component_name, Results.name
FROM Results
  INNER JOIN 
  Models 
    ON 
      Results.real_model_id=Models.id 
  INNER JOIN 
  Owners 
    ON 
      Models.owner_name=Owners.id 
WHERE 
  Results.status = 'In_queue' 
ORDER BY 
  Results.start_time
$$;


--
-- TOC entry 242 (class 1255 OID 16987)
-- Name: make_model(text, text, integer); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION make_model(label_in text, model_typename_in text, owner_name_in integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$  << outerblock >>
		DECLARE model Models%ROWTYPE;
        DECLARE id integer;
        BEGIN

        model.label = label_in;
        model.description = 'new created model';
        model.created = NOW();
        model.edited = NOW();
        model.typename = model_typename_in;
        model.owner_name = owner_name_in;
        model.id = get_new_id('Models');
        model.is_hidden = false;

        INSERT INTO Models VALUES(model.*);

        RETURN true;
        END; $$;


--
-- TOC entry 254 (class 1255 OID 18224)
-- Name: set_current_log_data(text, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_current_log_data(user_name text, content_in text) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE owners SET current_log = CONCAT(current_log,'\n' ,content_in) WHERE owner_name = user_name
$$;


--
-- TOC entry 252 (class 1255 OID 17305)
-- Name: set_model_component_valid(integer, text, boolean, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_model_component_valid(model_id_in integer, component_name_in text, is_valid_in boolean, name_in text) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE LazyModelComponentsViewsItems
  SET is_valid = is_valid_in::bool
  WHERE model_id = model_id_in AND
    model_view_item_pattern_id = ANY(SELECT id FROM ModelComponentsViewsSimpleItemPattern WHERE component_typename = component_name_in)
    and component_name = name_in
$$;



--
-- TOC entry 253 (class 1255 OID 17306)
-- Name: set_model_view_component(integer, integer, text, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_model_view_component(model_id_in integer, model_view_item_pattern_id_in integer, content_in text, name_in text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 		BEGIN
        UPDATE LazyModelComponentsViews
        SET content = content_in
        WHERE model_id = model_id_in AND model_view_component_id = model_view_item_pattern_id_in AND component_name=name_in;
        RETURN TRUE;
        END;
$$;


--
-- TOC entry 246 (class 1255 OID 16991)
-- Name: set_result_data(integer, text, real[], real[]); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_result_data(result_id_in integer, plot_label text, xdata_in real[], ydata_in real[]) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE plotdata SET is_standard = true, xdata = XData_in, ydata = YData_in WHERE results_id = result_id_in AND model_view_item_pattern_id 
   = (SELECT id FROM modelcomponentsviewsplotitempattern WHERE item_label = plot_label AND component_type = (SELECT component_name FROM results WHERE id = result_id_in))
$$;


--
-- TOC entry 259 (class 1255 OID 18189)
-- Name: set_result_data(integer, text, real[], real[], text[]); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_result_data(result_id_in integer, plot_label text, xdata_in real[], ydata_in real[], label_in text[]) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE plotdata SET is_standard = false, labels = label_in, xdata = XData_in, ydata = YData_in WHERE results_id = result_id_in AND model_view_item_pattern_id 
   = (SELECT id FROM modelcomponentsviewsplotitempattern WHERE item_label = plot_label AND component_type = (SELECT component_name FROM results WHERE id = result_id_in))
$$;


--
-- TOC entry 260 (class 1255 OID 18222)
-- Name: set_result_data_ext(integer, text, real[], real[], text[]); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_result_data_ext(result_id_in integer, plot_label text, xdata_in real[], ydata_in real[], label_in text[]) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE plotdata SET is_standard = false, labels = label_in, xdata = XData_in, ydata = YData_in WHERE results_id = result_id_in AND model_view_item_pattern_id 
   = (SELECT id FROM modelcomponentsviewsplotitempattern WHERE item_label = plot_label AND component_type = (SELECT component_name FROM results WHERE id = result_id_in))
$$;


--
-- TOC entry 243 (class 1255 OID 16992)
-- Name: set_result_progress(integer, double precision); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_result_progress(id_in integer, progress_in double precision) RETURNS void
    LANGUAGE sql
    AS $$

UPDATE results SET ready_status = progress_in WHERE id = id_in

$$;



--
-- TOC entry 245 (class 1255 OID 16993)
-- Name: set_result_status(integer, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_result_status(id_in integer, status_in text) RETURNS void
    LANGUAGE plpgsql
    AS $$
    BEGIN
  UPDATE results SET status = status_in WHERE id = id_in;
      
        if status_in='Done' THEN 
    UPDATE results SET done_time = NOW() WHERE id = id_in;
  END IF;
    end;
$$;


--
-- TOC entry 262 (class 1255 OID 16994)
-- Name: set_result_text_data(integer, text, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_result_text_data(result_id_in integer, item_label_in text, content_in text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE model_id_ int; pattern_id int; n_lazy int; name_comp text;
BEGIN
  
  SELECT INTO pattern_id id FROM modelcomponentsviewssimpleitempattern WHERE item_label = item_label_in AND
 component_typename = (SELECT component_name FROM results WHERE id = result_id_in);

   
SELECT INTO model_id_ real_model_id from results WHERE id = result_id_in;
SELECT INTO name_comp name from results WHERE id = result_id_in;

   UPDATE TextData
    SET data = content_in
  WHERE model_id = model_id_ AND model_view_item_pattern_id = pattern_id AND results_id = result_id_in;
    
END;
$$;



--
-- TOC entry 256 (class 1255 OID 18326)
-- Name: set_service_status(text, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_service_status(_ip text, _status text) RETURNS void
    LANGUAGE sql
    AS $$
UPDATE services SET status = _status WHERE ip = _ip
$$;


--
-- TOC entry 257 (class 1255 OID 18349)
-- Name: set_task_log_data(integer, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION set_task_log_data(_id integer, content_in text) RETURNS void
    LANGUAGE sql
    AS $$
  UPDATE results SET log = CONCAT(log,'\n' ,content_in) WHERE id = _id
$$;


--
-- TOC entry 244 (class 1255 OID 16995)
-- Name: update_lazy_item_data(integer, text); Type: FUNCTION; Schema: public; Owner: daisi_db_user
--

CREATE OR REPLACE FUNCTION update_lazy_item_data(item_id_in integer, content_in text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    DECLARE old_content TEXT;
    BEGIN 
        SELECT INTO old_content content FROM LazyModelComponentsViewsItems WHERE id = item_id_in;
      IF old_content=content_in THEN
        return false;
      END IF;
        UPDATE LazyModelComponentsViewsItems
        SET content = content_in
        WHERE id = item_id_in;
        return true;
    END;
$$;

CREATE OR REPLACE FUNCTION set_version(version_ integer) RETURNS void
LANGUAGE plpgsql
    AS $$
    BEGIN
  UPDATE options SET option = version_ WHERE description = 'version';
  if not found then
  insert into options(description, option) values('version', version_);  
  end if;
  END;
$$;

  CREATE OR REPLACE FUNCTION get_version() RETURNS integer
LANGUAGE sql
    AS $$
  select option from options WHERE description = 'version';
$$;
