DO $$
BEGIN
   IF NOT EXISTS (
      SELECT
      FROM   pg_catalog.pg_roles
      WHERE  rolname = 'daisi_db_user') THEN

      CREATE ROLE daisi_db_user LOGIN PASSWORD 'daisi_project_1234';
   END IF;
END
$$;

